Tutorial
========

``pyconf`` is a very simple configuration tool that performs variable
substitution on a given list of files. It is based on the popular unix-based
`Autoconf`_ configuration utility.

.. _Autoconf: https://en.wikipedia.org/wiki/Autoconf

The files, with the variables to replace, are identified with a ``.pin``
extension and are never modified by the tool. Upon performing the replacement
a new file is created in the same location as the original but with the
``.pin`` removed and any variables replaced.

Configuration file
------------------

The ``pyconf`` configuration file, always named ``pyconf.yml``, is a
`YAML`_ file that lists all the variable substitutions, pairs of name and
values, and the relative path of the  files to configure.

It consists of three main sections:

*   The first section, named ``pyconf``, holds metadata related to the tool
    itself, such as the ``version``, etc.
*   The second one, named ``variables``, lists all the variables and their
    corresponding values. 
*   The last one, named ``files`` lists the relative path of the files
    to perform the substitution. The files are listed without the ``.pin``
    extension. This section is optional and, if not present, no replacement
    will be performed (and a message will be displayed).

.. _YAML: https://en.wikipedia.org/wiki/YAML

Variables
---------

Variables in ``.pin`` files are enclosed by the ``@`` character. As such, the
character cannot be present in the variable name.

The name of the variable must be written in upper case and can only
contain alphanumeric characters and the underscore. 

The value can contain spaces but it will be stripped of any leading/trailing
space. If a value is surrounded by quotes, or double quotes, they are removed
and what is left becomes the actual value, including spaces.

.. code-block:: yaml

    variables:
      VAR_ONE: '   this value hast 3 leading and 3 trailing spaces   '
      VAR_TWO: "   this value hast 3 leading and 3 trailing spaces   "

To preserve the quotes as part of the value escape them as ``\'``.

.. code-block:: yaml

    variables:
      VAR_ONE: \'   quotes are preserved   \'

Quotes are required if the value contains any of these characters: ``:[]``.

.. code-block:: yaml

    variables:
      VAR_ONE: 'the value: 345'

Variable references in values
-----------------------------

It is possible to construct the value of a variable based on the value of
another variable defined anywhere in the file. This is achieved 
by enclosing the variable by ``${}``.
 
.. code-block:: yaml

    variables:
      VAR_ONE: value
      VAR_TWO: this ${VAR_ONE} uses another variable value

A complete example
------------------

A typical ``pyconf.yml`` configurationm file is illustrated below.

.. code-block:: yaml

    pyconf:
      version: 1.0
      author:  Alan
      date:    2018-07-04

    variables:
      APPLICATION_NAME:    nautilus
      APPLICATION_VERSION: 1.0.0
      OWNER:               alan
      EMAIL:               alan@gmail.com
      SOLUTION_NAME:       ${OWNER}-${APPLICATION_NAME}
      SOLUTION_VERSION:    ${APPLICATION_VERSION}-SNAPSHOT
      PRODUCT_NAME:        ${OWNER}/${APPLICATION_NAME}
      PRODUCT_VERSION:     ${APPLICATION_VERSION}.rc1
      SOLUTION_PROFILES:   '[production]'

    files:
    - makefile
    - REAME.md
    - bin/launch.sh
    - solution/pom.xml
    - config/launch.profile

And, assuming the above ``pyconf.yml`` is in the current working directory,
it is processed by just running ``pyconf``.

.. code-block:: bash

    $ pyconf

When used with the following ``bin/lunch.sh.pin`` file

.. code-block:: bash

    #!/usr/bin/env bash

    ./@APPLICATION_NAME@

The following ``bin/launch.sh`` will be created

.. code-block:: bash

    #!/usr/bin/env bash

    ./nautilus
