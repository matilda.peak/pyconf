matildapeak-pyconf
==================

Matilda Peak ``pyconf`` is a configuration utility written in Python 3.

``pyconf`` attempts to replicate the role of the unix `Autoconf`_ utility.
Where Autoconf relies on ``.in`` files and an ``autoconf.ac`` file ``pyconf``
relies on ``.pin`` files and a ``pyconf.yml`` file.

.. _Autoconf: https://en.wikipedia.org/wiki/Autoconf


Tutorial
--------

.. toctree::
   :maxdepth: 2

   tutorial
