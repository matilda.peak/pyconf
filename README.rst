An Autoconf-like Configuration Tool
===================================

.. image:: https://gitlab.com/matilda.peak/pyconf/badges/master/pipeline.svg
   :target: https://gitlab.com/matilda.peak/pyconf
   :alt: Pipeline Status (pyconf)

.. image:: https://gitlab.com/matilda.peak/pyconf/badges/master/coverage.svg
   :target: https://gitlab.com/matilda.peak/pyconf
   :alt: Coverage Report (pyconf)

.. image:: https://badge.fury.io/py/matildapeak-pyconf.svg
   :target: https://badge.fury.io/py/matildapeak-pyconf
   :alt: PyPI package (latest)

.. image:: //readthedocs.org/projects/pyconf/badge/?version=latest
   :target: https://pyconf.readthedocs.io/en/latest/?badge=latest
   :alt: Documentation Status (pyconf)

``pyconf`` attempts to replicate the role of the unix `Autoconf`_ utility.
Where Autoconf relies on ``.in`` files and an ``autoconf.ac`` file ``pyconf``
relies on ``.pin`` files and a ``pyconf.yml`` file.

.. _Autoconf: https://en.wikipedia.org/wiki/Autoconf

Installation
------------

Pyconf is published on `PyPI`_ and can be installed from
there::

    pip install matildapeak-pyconf

.. _PyPI: https://pypi.org/project/matildapeak-pyconf

Documentation
-------------

Pyconf documentation is available on `Read The Docs`_.

.. _Read The Docs: https://pyconf.readthedocs.io/en/latest/

Get in touch
------------

- Report bugs, suggest features or view the source code `on GitLab`_.

.. _on GitLab: https://gitlab.com/matilda.peak/pyconf
