"""
Unit tests for Configuration class in pyconf.py
"""

import unittest
from unittest.mock import patch

from pyconf.pyconf import Configuration


class ConfigurationTest(unittest.TestCase):
    """Unit tests the Configuration class that loads the yaml file"""

    # -------------------------------------------------------------------------
    @patch('pyconf.pyconf.FileManager', autospec=True)
    @patch('pyconf.pyconf.Replacer', autospec=True)
    @patch('pyconf.pyconf.VariableCollector', autospec=True)
    @patch('pyconf.pyconf.Loader', autospec=True)
    def test_apply(
            self,
            mock_loader,
            mock_variable_collector,
            mock_replacer,
            mock_file_manager):
        """Capability test: calling apply performs the expected calls"""
        files = [
            'makefile',
            'README.md',
            'bin/launch.sh',
            'solution/pom.xml'
        ]
        variables = {
            'APPLICATION_NAME': 'nautilus',
            'APPLICATION_VERSION': '1.0.0',
            'SOLUTION_NAME': '${APPLICATION_NAME}-${APPLICATION_VERSION}'
        }
        rendered_variables = {
            'APPLICATION_NAME': 'nautilus',
            'APPLICATION_VERSION': '1.0.0',
            'SOLUTION_NAME': 'nautilus-1.0.0'
        }

        mock_loader.return_value.get_files.return_value = files
        mock_loader.return_value.get_variables.return_value = variables
        mock_variable_collector.return_value.get_variables.return_value = \
            rendered_variables

        Configuration().apply()

        mock_loader.return_value.get_variables.assert_any_call()
        mock_loader.return_value.get_files.assert_any_call()
        mock_variable_collector.assert_called_once_with(variables)
        mock_variable_collector.return_value.get_variables.assert_any_call()
        mock_replacer.assert_called_once_with(rendered_variables)
        mock_file_manager.assert_called_once_with(files)
        mock_file_manager.return_value.replace.assert_called_once_with(
            mock_replacer.return_value)

    # -------------------------------------------------------------------------
    #
    # -------------------------------------------------------------------------
    @patch('pyconf.pyconf.FileManager', autospec=True)
    @patch('pyconf.pyconf.Replacer', autospec=True)
    @patch('pyconf.pyconf.VariableCollector', autospec=True)
    @patch('pyconf.pyconf.Loader', autospec=True)
    def test_can_load_if_files_section_missing(
            self,
            mock_loader,
            mock_variable_collector,
            mock_replacer,
            mock_file_manager):
        """
        Capability test: instance can be created if the 'files' section
        is missing in the configuration file
        """
        variables = {
            'APPLICATION_NAME': 'nautilus',
        }
        rendered_variables = {
            'APPLICATION_NAME': 'nautilus',
        }

        mock_loader.return_value.get_files.return_value = []
        mock_loader.return_value.get_variables.return_value = variables
        mock_variable_collector.return_value.get_variables.return_value = \
            rendered_variables

        Configuration().apply()

        mock_loader.return_value.get_variables.assert_any_call()
        mock_loader.return_value.get_files.assert_any_call()
        mock_variable_collector.assert_called_once_with(variables)
        mock_variable_collector.return_value.get_variables.assert_any_call()
        mock_replacer.assert_not_called()
        mock_file_manager.assert_not_called()

    # -------------------------------------------------------------------------
    @patch('pyconf.pyconf.Loader', autospec=True)
    def test_cannot_load_if_variables_section_missing(self, mock_loader):
        """
        Exception test: loading fails if the 'variables' section
        is missing in the configuration file
        """
        mock_loader.return_value.get_files.return_value = [
            'makefile',
            'README.md',
            'bin/launch.sh',
            'solution/pom.xml'
        ]
        mock_loader.return_value.get_variables.return_value = {}
        mock_loader.return_value.get_full_path.return_value = 'pyconf.yaml'

        with self.assertRaisesRegex(
                RuntimeError,
                'Missing variables section in configuration file'):
            Configuration()

        mock_loader.return_value.get_variables.assert_any_call()
        mock_loader.return_value.get_full_path.assert_any_call()
        mock_loader.return_value.get_files.assert_not_called()

# -----------------------------------------------------------------------------
# Helps individually execution of tests
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    unittest.main()
