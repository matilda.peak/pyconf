"""
Unit tests for Loader class in pyconf.py
"""

import unittest
from unittest.mock import patch, mock_open, call

import yaml

from pyconf.pyconf import Loader


@patch('pyconf.pyconf.os')
class LoaderTest(unittest.TestCase):
    """Unit tests the Loader class that loads the yaml file"""

    with open('test/data/valid-pyconf.yaml', 'r') as file:
        valid_contents_yaml = file.read()

    with open('test/data/valid-pyconf.yml', 'r') as file:
        valid_contents_yml = file.read()

    with open('test/data/valid-pyconf-non-string-values.yml', 'r') as file:
        valid_contents_non_string_values_yml = file.read()

    with open('test/data/valid-pyconf-with-no-files-section.yml', 'r') as file:
        valid_yml_with_no_files_section = file.read()

    with open('test/data/valid-pyconf-with-empty-files-section.yml', 'r') as file:
        valid_yml_with_empty_files_section = file.read()

    with open('test/data/invalid-pyconf.yaml', 'r') as file:
        invalid_contents = file.read()

    # -------------------------------------------------------------------------
    def test_load_yml_file_extension(self, mock_os):
        """Capability test: if the '.yml' file is found, its contents are loaded
        as the valid configuration
        """
        mock_os.getcwd.return_value = 'dummy'
        # '.yml' => True, '.yaml' => false
        mock_os.path.isfile.side_effect = [True, False]

        with patch(
                'builtins.open',
                mock_open(read_data=self.valid_contents_yml)) as cm:
            loader = Loader()

        mock_os.getcwd.assert_any_call()
        # Both files are always checked, as having both is an error
        mock_os.path.isfile.assert_has_calls(
            [
                call('pyconf.yml'),
                call('pyconf.yaml')
            ])

        cm.assert_called_once_with('pyconf.yml', 'r')

        self.assertEqual(loader.filename, 'pyconf.yml')

        self.assertIsNotNone(loader.get_contents())
        self.assertEqual(
            loader.get_variables(),
            {
                'APPLICATION_NAME': 'nautilus',
                'APPLICATION_VERSION': '2.0.0',
                'OWNER': 'alan',
                'EMAIL': 'alan@gmail.com',
                'SOLUTION_NAME': '${OWNER}-${APPLICATION_NAME}',
                'SOLUTION_VERSION': '${APPLICATION_VERSION}-SNAPSHOT',
                'PRODUCT_NAME': '${OWNER}/${APPLICATION_NAME}',
                'PRODUCT_VERSION': '${APPLICATION_VERSION}.rc1',
                'SOLUTION_PROFILES': '[production]'
            })
        self.assertEqual(
            loader.get_files(),
            [
                'makefile',
                'README.md',
                'bin/launch.sh',
                'solution/pom.xml'
            ])

    # -------------------------------------------------------------------------
    def test_load_yaml_configuration_file(self, mock_os):
        """Capability test: if the '.yml' file is missing and a '.yaml' is found,
         its contents are loaded as the valid configuration
        """
        mock_os.getcwd.return_value = 'dummy'
        # '.yml' => True, '.yaml' => false
        mock_os.path.isfile.side_effect = [False, True]

        with patch(
                'builtins.open',
                mock_open(read_data=self.valid_contents_yaml)) as cm:
            loader = Loader()

        mock_os.getcwd.assert_any_call()
        mock_os.path.isfile.assert_has_calls(
            [
                call('pyconf.yml'),
                call('pyconf.yaml')
            ])

        cm.assert_called_once_with('pyconf.yaml', 'r')

        self.assertEqual(loader.filename, 'pyconf.yaml')

        self.assertIsNotNone(loader.get_contents())
        self.assertEqual(
            loader.get_variables(),
            {
                'APPLICATION_NAME': 'nautilus',
                'APPLICATION_VERSION': '1.0.0',
                'OWNER': 'alan',
                'EMAIL': 'alan@gmail.com',
                'SOLUTION_NAME': '${OWNER}-${APPLICATION_NAME}',
                'SOLUTION_VERSION': '${APPLICATION_VERSION}-SNAPSHOT',
                'PRODUCT_NAME': '${OWNER}/${APPLICATION_NAME}',
                'PRODUCT_VERSION': '${APPLICATION_VERSION}.rc1',
                'SOLUTION_PROFILES': '[production]'
            })
        self.assertEqual(
            loader.get_files(),
            [
                'makefile',
                'README.md',
                'bin/launch.sh',
                'solution/pom.xml'
            ])

    # -------------------------------------------------------------------------
    def test_load_yml_non_string_values(self, mock_os):
        """Capability test: the contents of the file are read as string and
        they are not resolved into other types
        """
        mock_os.getcwd.return_value = 'dummy'
        # '.yml' => True, '.yaml' => false
        mock_os.path.isfile.side_effect = [True, False]

        with patch(
                'builtins.open',
                mock_open(read_data=self.valid_contents_non_string_values_yml)) as cm:
            loader = Loader()

        mock_os.getcwd.assert_any_call()
        # Both files are always checked, as having both is an error
        mock_os.path.isfile.assert_has_calls(
            [
                call('pyconf.yml'),
                call('pyconf.yaml')
            ])

        cm.assert_called_once_with('pyconf.yml', 'r')

        self.assertEqual(loader.filename, 'pyconf.yml')

        self.assertIsNotNone(loader.get_contents())
        self.assertEqual(
            loader.get_variables(),
            {
                'APPLICATION_NAME': 'nautilus',
                'APPLICATION_VERSION': '2018.1',
                'OWNER': 'alan',
                'OTHER': '-5.4',
                'DATE': '2018-01-01T00:00:00Z',
                'EMAIL': 'alan@gmail.com',
                'BOOLEAN': 'Yes',
                'SOLUTION_NAME': '${OWNER}-${APPLICATION_NAME}',
                'SOLUTION_VERSION': '${APPLICATION_VERSION}-SNAPSHOT',
                'PRODUCT_NAME': '${OWNER}/${APPLICATION_NAME}',
                'PRODUCT_VERSION': '${APPLICATION_VERSION}.rc1',
                'SOLUTION_PROFILES': '[production]'
            })
        self.assertEqual(
            loader.get_files(),
            [
                'makefile',
                'README.md',
                'bin/launch.sh',
                'solution/pom.xml'
            ])

    # -------------------------------------------------------------------------
    def test_load_yml_with_no_files_section(self, mock_os):
        """Capability test: if the '.yml' file is found, its contents are loaded
        as the valid configuration even if the 'files' section is missing.
        """
        mock_os.getcwd.return_value = 'dummy'
        # '.yml' => True, '.yaml' => false
        mock_os.path.isfile.side_effect = [True, False]

        with patch(
                'builtins.open',
                mock_open(read_data=self.valid_yml_with_no_files_section)) as cm:
            loader = Loader()

        mock_os.getcwd.assert_any_call()
        # Both files are always checked, as having both is an error
        mock_os.path.isfile.assert_has_calls(
            [
                call('pyconf.yml'),
                call('pyconf.yaml')
            ])

        cm.assert_called_once_with('pyconf.yml', 'r')

        self.assertEqual(loader.filename, 'pyconf.yml')

        self.assertIsNotNone(loader.get_contents())
        self.assertEqual(
            loader.get_variables(),
            {
                'APPLICATION_NAME': 'nautilus',
                'APPLICATION_VERSION': '3.0.0',
                'OWNER': 'alan',
                'EMAIL': 'alan@gmail.com',
                'SOLUTION_NAME': '${OWNER}-${APPLICATION_NAME}',
                'SOLUTION_VERSION': '${APPLICATION_VERSION}-SNAPSHOT',
                'PRODUCT_NAME': '${OWNER}/${APPLICATION_NAME}',
                'PRODUCT_VERSION': '${APPLICATION_VERSION}.rc1',
                'SOLUTION_PROFILES': '[production]'
            })
        self.assertEqual(loader.get_files(), [])

    # -------------------------------------------------------------------------
    def test_load_yml_with_empty_files_section(self, mock_os):
        """Capability test: if the '.yml' file is found, its contents are loaded
        as the valid configuration even if the 'files' section is empty.
        """
        mock_os.getcwd.return_value = 'dummy'
        # '.yml' => True, '.yaml' => false
        mock_os.path.isfile.side_effect = [True, False]

        with patch(
                'builtins.open',
                mock_open(read_data=self.valid_yml_with_empty_files_section)) as cm:
            loader = Loader()

        mock_os.getcwd.assert_any_call()
        # Both files are always checked, as having both is an error
        mock_os.path.isfile.assert_has_calls(
            [
                call('pyconf.yml'),
                call('pyconf.yaml')
            ])

        cm.assert_called_once_with('pyconf.yml', 'r')

        self.assertEqual(loader.filename, 'pyconf.yml')

        self.assertIsNotNone(loader.get_contents())
        self.assertEqual(
            loader.get_variables(),
            {
                'APPLICATION_NAME': 'nautilus',
                'APPLICATION_VERSION': '3.0.0',
                'OWNER': 'alan',
                'EMAIL': 'alan@gmail.com',
                'SOLUTION_NAME': '${OWNER}-${APPLICATION_NAME}',
                'SOLUTION_VERSION': '${APPLICATION_VERSION}-SNAPSHOT',
                'PRODUCT_NAME': '${OWNER}/${APPLICATION_NAME}',
                'PRODUCT_VERSION': '${APPLICATION_VERSION}.rc1',
                'SOLUTION_PROFILES': '[production]'
            })
        self.assertEqual(loader.get_files(), [])

    # -------------------------------------------------------------------------
    def test_load_fails_if_both_yml_and_yaml_are_present(self, mock_os):
        """Exception test: if 2 files, one with .yaml and another with .yml
        extensions are present, load must fail as it is ambiguous
        """
        mock_os.getcwd.return_value = 'dummy'
        # '.yml' => True, '.yaml' => True
        mock_os.path.isfile.side_effect = [True, True]

        with self.assertRaisesRegex(
                RuntimeError,
                "Found configuration file pyconf with both extensions '.yml' "
                "and '.yaml' in dummy. You can have one or the other, "
                "but not both"):
            Loader()

        mock_os.getcwd.assert_any_call()
        mock_os.path.isfile.assert_has_calls(
            [
                call('pyconf.yml'),
                call('pyconf.yaml')
            ])

    # -------------------------------------------------------------------------
    def test_load_fails_if_file_not_found(self, mock_os):
        """Exception tests: the process fails if there is not configuration file
        """
        mock_os.getcwd.return_value = 'dummy'
        mock_os.path.isfile.side_effect = [False, False]

        with self.assertRaisesRegex(
                RuntimeError,
                'Missing configuration file pyconf.yml in working directory'
                ' dummy'):
            Loader()

        mock_os.getcwd.assert_any_call()
        mock_os.path.isfile.assert_has_calls(
            [
                call('pyconf.yml'),
                call('pyconf.yaml')
            ])

    # -------------------------------------------------------------------------
    def test_load_fails_if_file_is_invalid(self, mock_os):
        """Exception tests: the process fails if the configuration file is not a
        valid YAML file
        """
        mock_os.getcwd.return_value = 'dummy'
        mock_os.path.isfile.side_effect = [False, True]

        with self.assertRaises(yaml.YAMLError):
            with patch('builtins.open',
                       mock_open(
                           read_data=self.invalid_contents)) as cm:
                Loader()

        mock_os.getcwd.assert_any_call()
        mock_os.path.isfile.assert_has_calls(
            [
                call('pyconf.yml'),
                call('pyconf.yaml')
            ])
        cm.call_args('pyconf.yaml', 'r')


# -----------------------------------------------------------------------------
# Helps individually execution of tests
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    unittest.main()
