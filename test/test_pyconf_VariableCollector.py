"""
Unit tests for VariableCollector class in pyconf.py
"""

import unittest

from pyconf.pyconf import VariableCollector


class VariableCollectorTest(unittest.TestCase):
    """Unit tests for the VarCollector class"""

    # -------------------------------------------------------------------------
    def test_simple_variables_are_collected(self):
        """Capability test: collector handles variable definitions without
         references nor embedded python just fine
        """

        variables = {
            'APPLICATION_VERSION': '1.0.0',
            'OWNER': 'someone',
            'EMAIL': 'someone@gmail.com'
        }

        collector = VariableCollector(variables)

        self.assertEqual(
            collector.get_variables()['APPLICATION_VERSION'], '1.0.0')
        self.assertEqual(
            collector.get_variables()['OWNER'], 'someone')
        self.assertEqual(
            collector.get_variables()['EMAIL'], 'someone@gmail.com')

    # -------------------------------------------------------------------------
    def test_references_are_handled(self):
        """Capability test: collector handles variable definitions with
         references in the values. Order is irrelevant
        """

        variables = {
            'APPLICATION_NAME': 'nautilus',
            'APPLICATION_VERSION': '1.0.0',
            'SOLUTION_NAME': '${OWNER}-${APPLICATION_NAME}',
            'SOLUTION_VERSION': '${APPLICATION_VERSION}-SNAPSHOT',
            'PRODUCT_NAME': '${OWNER}/${APPLICATION_NAME}',
            'PRODUCT_VERSION': '${APPLICATION_VERSION}.rc1',
            'OWNER': 'someone',
            'EMAIL': 'someone@gmail.com'
        }

        collector = VariableCollector(variables)

        self.assertEqual(
            collector.get_variables()['APPLICATION_NAME'], 'nautilus')
        self.assertEqual(
            collector.get_variables()['APPLICATION_VERSION'], '1.0.0')
        self.assertEqual(
            collector.get_variables()['OWNER'], 'someone')
        self.assertEqual(
            collector.get_variables()['EMAIL'], 'someone@gmail.com')
        self.assertEqual(
            collector.get_variables()['SOLUTION_NAME'], 'someone-nautilus')
        self.assertEqual(
            collector.get_variables()['SOLUTION_VERSION'], '1.0.0-SNAPSHOT')
        self.assertEqual(
            collector.get_variables()['PRODUCT_NAME'], 'someone/nautilus')
        self.assertEqual(
            collector.get_variables()['PRODUCT_VERSION'], '1.0.0.rc1')

    # -------------------------------------------------------------------------
    # def test_values_from_embedded_python(self):
    #     """
    #     Capability test: collector can create values from snippets of embedded
    #      python code
    #     """
    #
    #     # Arrange
    #     variables = {
    #         'BUILD_NUMBER': '^import time; time.timezone^',
    #         'BUILD_DATE': '^import time; '
    #                       'time.strftime('
    #                       '"%Y-%m-%dT%H:%M:%SZ", time.gmtime(0))^'
    #     }
    #
    #     # Act
    #     collector = VarCollector(variables)
    #
    #     # Assert
    #     self.assertEqual(collector.variables['BUILD_NUMBER'], time.timezone)
    #     self.assertEqual(collector.variables['BUILD_NUMBER'],
    #                      '1970-01-01T00:00:00Z')
    #
    #
    # def test_collector_fails_if_value_with_embedded_code_fails(self):
    #     """Exception test: collector cannot be created if the embedded
    #     python code that produces the value fails with an error
    #     """

    # -------------------------------------------------------------------------
    def test_fails_if_back_reference_is_missing(self):
        """Exception test: collector cannot be created if a value references a
         variable not defined in the file
        """

        variables = {
            'APPLICATION_NAME': 'nautilus',
            'APPLICATION_VERSION': '1.0.0',
            'PRODUCT_VERSION': '${UNDEFINED}.rc1'
        }

        with self.assertRaisesRegex(
                RuntimeError,
                "Undefined variable referenced by PRODUCT_VERSION:"
                " 'UNDEFINED'"):
            VariableCollector(variables)

    # -------------------------------------------------------------------------
    def test_fails_if_no_variables(self):
        """Exception test: collector cannot be created if supplied with an empty
         dictionary
        """
        variables = dict()

        with self.assertRaisesRegex(
                RuntimeError,
                'Missing variables section in configuration file'):
            VariableCollector(variables)

    # -------------------------------------------------------------------------
    def test_fails_if_variable_name_is_invalid(self):
        """Exception test: collector cannot be created if supplied with a
         dictionary that contains invalid names
        """
        variables = {
            'name_in_lower': 'value',
            'NAME WITH SPACES': 'value',
            'NAME_WITH_NON_ALPHA+': 'value',
            '': 'value'  # Empty name
        }

        with self.assertRaises(RuntimeError) as cm:
            VariableCollector(variables)

        self.assertEqual(str(cm.exception),
                         'Invalid variable name found: {0}'.format(
                             list(variables.keys())))


# -----------------------------------------------------------------------------
# Helps individually execution of tests
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    unittest.main()
