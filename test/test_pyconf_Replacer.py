"""
Unit tests for Replacer class in pyconf.py
"""

import unittest
from unittest.mock import patch
from pyconf.pyconf import Replacer


class ReplacerTest(unittest.TestCase):
    """Unit tests for the Replacer class"""

    # -------------------------------------------------------------------------
    def test_identifies_variables_using_defined_markers(self):
        """Capability test: variables in the file are identified by being
        enclosed by '@' characters
        """

        contents = 'This is a @VAR_NAME_1@ test with\n' \
                   'multiple @VAR_NAME_2@ variables in multiple\n' \
                   'lines @VAR_NAME_3@ @VAR_NAME_4@'

        markers_iterator = iter(
            Replacer(dict()).find_variable_markers(contents))

        self.assertEqual(next(markers_iterator), 'VAR_NAME_1')
        self.assertEqual(next(markers_iterator), 'VAR_NAME_2')
        self.assertEqual(next(markers_iterator), 'VAR_NAME_3')
        self.assertEqual(next(markers_iterator), 'VAR_NAME_4')
        with self.assertRaises(StopIteration):
            next(markers_iterator)

    # -------------------------------------------------------------------------
    def test_handles_multiple_copies_same_variable_marker(self):
        """Capability test: when looking for all existing variable markers in the
         text, duplicated markers are only counted once
        """

        contents = 'This is a @VAR_NAME_1@ test with\n' \
                   'multiple @VAR_NAME_2@ variables in multiple\n' \
                   'lines @VAR_NAME_1@ @VAR_NAME_2@'

        markers_iterator = iter(
            Replacer(dict()).find_variable_markers(contents))

        self.assertEqual(next(markers_iterator), 'VAR_NAME_1')
        self.assertEqual(next(markers_iterator), 'VAR_NAME_2')
        with self.assertRaises(StopIteration):
            next(markers_iterator)

    # -------------------------------------------------------------------------
    def test_substitution_against_given_config(self):
        """Capability test: the output content is the result of performing a
         variable substitution on the input against the given configuration
        """

        contents = 'This is a @VAR_NAME_1@ test with\n' \
                   'multiple @VAR_NAME_2@ variables in multiple\n' \
                   'lines @VAR_NAME_3@ @VAR_NAME_1@'

        variables = {
            'VAR_NAME_1': 'value_1',
            'VAR_NAME_2': 'value_2',
            'VAR_NAME_3': 'value_3',
        }

        result = Replacer(variables).do_variable_substitution(contents)

        self.assertEqual(
            result,
            'This is a value_1 test with\n'
            'multiple value_2 variables in multiple\n'
            'lines value_3 value_1'
        )

    # -------------------------------------------------------------------------
    @patch('pyconf.pyconf.os')
    def test_substitution_against_environment(self, mock_os):
        """Capability test: the output content is the result of performing a
         variable substitution on the input against the variables in the
         environment
        """

        contents = 'This is a @VAR_NAME_1@ test with\n' \
                   'multiple @VAR_NAME_2@ variables in multiple\n' \
                   'lines @VAR_NAME_3@ @VAR_NAME_1@'

        variables = {}

        mock_os.environ = {
            'VAR_NAME_1': 'value_1_env',
            'VAR_NAME_2': 'value_2_env',
            'VAR_NAME_3': 'value_3_env',
        }

        result = Replacer(variables).do_variable_substitution(contents)

        self.assertEqual(
            result,
            'This is a value_1_env test with\n'
            'multiple value_2_env variables in multiple\n'
            'lines value_3_env value_1_env'
        )

    # -------------------------------------------------------------------------
    @patch('pyconf.pyconf.os')
    def test_substitution_first_config_and_then_environment(self, mock_os):
        """Capability test: the output content is the result of performing a
         variable substitution on the input, by first looking up in the
         configuration and then in the environment.
        """

        contents = 'This is a @VAR_NAME_1@ test with\n' \
                   'multiple @VAR_NAME_2@ variables in multiple\n' \
                   'lines @VAR_NAME_3@ @VAR_NAME_1@\n' \
                   'and also @VAR_NAME_4@ against the environment @VAR_NAME_5@'

        variables = {
            'VAR_NAME_1': 'value_1',
            'VAR_NAME_3': 'value_3',
            'VAR_NAME_4': 'value_4',
        }

        mock_os.environ = {
            'VAR_NAME_1': 'value_1_env',
            'VAR_NAME_2': 'value_2_env',
            'VAR_NAME_3': 'value_3_env',
            'VAR_NAME_4': 'value_4_env',
            'VAR_NAME_5': 'value_5_env',
        }

        result = Replacer(variables).do_variable_substitution(contents)

        self.assertEqual(
            result,
            'This is a value_1 test with\n'
            'multiple value_2_env variables in multiple\n'
            'lines value_3 value_1\n'
            'and also value_4 against the environment value_5_env'
        )

    # -------------------------------------------------------------------------
    @patch('pyconf.pyconf.os')
    def test_fails_if_variable_is_not_in_config_nor_environment(self, mock_os):
        """Exception test: process fails if a variable in a file is missing from
         the configuration.
        """

        contents = 'First line @VAR_NAME_1@ is this\n' \
                   'Second line @VAR_NAME_2@ is this\n' \
                   '@VAR_NAME_3@ the third line'

        variables = {
            'VAR_NAME_1': 'value_1'
        }

        mock_os.environ = {
            'VAR_NAME_4': 'value_4_env',
            'VAR_NAME_5': 'value_5_env',
        }

        with self.assertRaisesRegex(
                RuntimeError,
                "Missing variables \['VAR_NAME_2', 'VAR_NAME_3'\]"):
            Replacer(variables).do_variable_substitution(contents)


# -----------------------------------------------------------------------------
# Helps individually execution of tests
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    unittest.main()
