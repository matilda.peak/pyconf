"""
Unit tests for FileWriter class in pyconf.py
"""

import unittest
from unittest.mock import patch, mock_open, call

from pyconf.pyconf import FileManager


# -----------------------------------------------------------------------------
def mock_open_next(mock):
    """Implementation of __next__ for mock_open internal handler
    :return: next line in the 'file'
    """
    line = mock.readline()
    if not line.strip():
        raise StopIteration
    else:
        return line


# ------------------------------------------------------------------------------
class FileManagerTest(unittest.TestCase):
    """Unit tests for the FileWriter class"""

    # --------------------------------------------------------------------------
    def test_constructor_appends_pin_extension_to_files_in_configuration(self):
        """Capability test: on creating the manager, it maps every file to their
        corresponding '.pin' file
        """
        files = ['makefile', 'README.md', 'bin/launch.sh', 'solution/pom.xml']

        manager = FileManager(files)

        self.assertEqual(
            manager.mappings,
            {
                'makefile': 'makefile.pin',
                'README.md': 'README.md.pin',
                'bin/launch.sh': 'bin/launch.sh.pin',
                'solution/pom.xml': 'solution/pom.xml.pin'
            })

    # --------------------------------------------------------------------------
    @patch.object(FileManager, 'replace_file')
    @patch('pyconf.pyconf.Replacer', autospec=True)
    def test_applies_replace_to_all_configured_files(
            self,
            mock_replacer,
            mock_replace_file_method):
        """Capability test: calling 'replace' calls 'replace_file' on the '.pin'
        file and its corresponding target file from the list of files given
        in the constructor
        """
        manager = FileManager(
            [
                'makefile', 'README.md', 'bin/launch.sh', 'solution/pom.xml'
            ])

        manager.replace(mock_replacer)

        mock_replace_file_method.assert_any_call(
            mock_replacer, 'makefile.pin', 'makefile'),
        mock_replace_file_method.assert_any_call(
            mock_replacer, 'README.md.pin', 'README.md'),
        mock_replace_file_method.assert_any_call(
            mock_replacer, 'bin/launch.sh.pin', 'bin/launch.sh'),
        mock_replace_file_method.assert_any_call(
            mock_replacer, 'solution/pom.xml.pin', 'solution/pom.xml'),

    # --------------------------------------------------------------------------
    @patch.object(FileManager, 'replace_file')
    @patch('pyconf.pyconf.Replacer', autospec=True)
    def test_file_order_is_honored(
            self,
            mock_replacer,
            mock_replace_file_method):
        """Capability test: the files are processed in the same order as found
        in the configuration file
        """
        manager = FileManager(
            [
                'makefile', 'README.md', 'bin/launch.sh', 'solution/pom.xml'
            ])

        manager.replace(mock_replacer)

        mock_replace_file_method.assert_has_calls(
            [
                call(mock_replacer, 'makefile.pin', 'makefile'),
                call(mock_replacer, 'README.md.pin', 'README.md'),
                call(mock_replacer, 'bin/launch.sh.pin', 'bin/launch.sh'),
                call(mock_replacer, 'solution/pom.xml.pin', 'solution/pom.xml')
            ])

    # --------------------------------------------------------------------------
    @patch('pyconf.pyconf.Replacer', autospec=True)
    def test_pin_files_are_read(self, mock_replacer):
        """Capability test: when replacing the contents, the '.pin' file with the
        original data is only read
        """
        with patch('builtins.open', mock_open()) as cm:
            FileManager.replace_file(
                mock_replacer,
                'bin/launch.sh.pin',
                'bin/launch.sh')

        # Make sure that the original file is opened once and only open for
        # reading. Also 'call_args_list' is a list of tuples of tuples
        # (positional args, named args)
        flags = [flag for filename, flag in
                 (args[0] for args in cm.call_args_list)
                 if filename == 'bin/launch.sh.pin']

        self.assertEqual(flags, ['r'])

    # -------------------------------------------------------------------------
    @patch('pyconf.pyconf.Replacer', autospec=True)
    def test_pinless_files_are_written(self, mock_replacer):
        """Capability test: the list of files in the configuration are the
         target filenames
        """
        with patch('builtins.open', mock_open()) as cm:
            FileManager.replace_file(
                mock_replacer,
                'bin/launch.sh.pin',
                'bin/launch.sh')

        # Make sure that the original file is opened once and only open for
        # reading. Also 'call_args_list' is a list of tuples of tuples
        # (positional args, named args)
        flags = [flag for filename, flag in
                 (args[0] for args in cm.call_args_list)
                 if filename == 'bin/launch.sh']

        self.assertEqual(flags, ['w'])

    # -------------------------------------------------------------------------
    @patch('pyconf.pyconf.Replacer', autospec=True)
    def test_on_each_line_replacing_is_executed_and_result_written(
            self,
            mock_replacer):
        """Capability test: for a given file, its contents are read, passed on
        to the replacer in a line by line fashion and the result is then written
        one by one.

        Warning: mock_open does not support iteration
        (http://bugs.python.org/issue21258) and there for something like
        'for line in file:' will fail. Was forced to patch the mock object using
         http://stackoverflow.com/a/24779923

         'mock_open.return_value.readline()' is already available
        """
        contents = 'this is the first line\n' \
                   'this is the second one\n' \
                   'this is the third one\n'
        cm = mock_open(read_data=contents)
        cm.return_value.__iter__ = lambda ss: ss
        cm.return_value.__next__ = mock_open_next

        with patch('builtins.open', cm) as cm:
            FileManager.replace_file(
                mock_replacer,
                'bin/launch.sh.pin',
                'bin/launch.sh')

        # Replacing line by line in several calls
        self.assertEqual(
            mock_replacer.do_variable_substitution.call_args_list,
            [
                call('this is the first line\n'),
                call('this is the second one\n'),
                call('this is the third one\n')
            ])
        # The mock_open.return_value is the mock file on which read and write
        # happens. The mock __call()__ returns the return_value.
        self.assertEqual(len(cm().readline.call_args_list), 4)  # 3 + StopIter
        self.assertEqual(len(cm().write.call_args_list), 3)  # only write 3

    # -------------------------------------------------------------------------
    def test_fails_if_no_files_provided_in_configuration(self):
        """Exception test: instance cannot be created if given an empty list
        of files
        """
        empty = list()

        with self.assertRaisesRegex(
                RuntimeError,
                'Missing files to configure'):
            FileManager(empty)

    # -------------------------------------------------------------------------
    @patch.object(FileManager, 'replace_file')
    @patch('pyconf.pyconf.Replacer', autospec=True)
    def test_fails_fast(self, mock_replacer, mock_replace_file_method):
        """Exception test: once an error is found, the whole process stops"""
        manager = FileManager(
            [
                'makefile', 'README.md', 'bin/launch.sh', 'solution/pom.xml'
            ])
        mock_replace_file_method.side_effect = [
            1, IOError("File not found: README.md")]

        with self.assertRaisesRegex(
                RuntimeError,
                "File not found: README.md"):
            manager.replace(mock_replacer)

        # wish mock had assert_not_call(args)
        self.assertEqual(
            mock_replace_file_method.call_args_list,
            [
                call(mock_replacer, 'makefile.pin', 'makefile'),
                call(mock_replacer, 'README.md.pin', 'README.md')
            ])


# -----------------------------------------------------------------------------
# Helps individually execution of tests
# -----------------------------------------------------------------------------
if __name__ == '__main__':
    unittest.main()
